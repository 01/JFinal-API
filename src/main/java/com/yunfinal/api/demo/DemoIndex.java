package com.yunfinal.api.demo;

import com.jfinal.core.Controller;
import com.yunfinal.api.service.doc.ApiDoc;
import com.yunfinal.api.service.doc.ApiReportSocket;

/**
 * 首页处理
 */
public class DemoIndex extends Controller {

    @ApiDoc("默认接口")
    public void index() {
        redirect("/doc");
    }

    @ApiDoc("测试系统")
    public void test(){
        set("d", getParaMap());
        renderTemplate("/test.html");
    }

    @ApiDoc("系统API文档")
    public void doc(){
        renderTemplate("/doc.html");
    }

    @ApiDoc("请求实时报告")
    public void report(){
        //如果是Nginx代理的话，注意Nginx的Socket配置
        set("wsUrl", ApiReportSocket.getWsUrl(this.getRequest()));
        renderTemplate("/report.html");
    }


}

package com.yunfinal.api.service;

import com.jfinal.log.Log;

/**
 * 服务层调用， 常用操作抽取为父类
 *
 * @author 杜福忠
 *
 */
public class ApiService {

    protected static ApiCheck kit = ApiCheck.ME;

    /**
     * 日志
     */
    protected final Log log = Log.getLog(getClass());

    /**
     * 错误直接返回
     *
     * @param code
     * @param msg
     */
    protected void returnError(String code, String msg) {
        throw new ApiException(code, msg);
    }

    /**
     * 错误直接返回
     *
     * @param code
     * @param msg
     * @param data
     */
    protected void returnError(String code, String msg, Object data) {
        throw new ApiException(code, msg, data);
    }

    public boolean isNotBlank(Object str) {
        return !isBlank(str);
    }

    /**
     * 字符串为 null 或者内部字符全部为 ' ' '\t' '\n' '\r' 这四类字符时返回 true
     */
    public boolean isBlank(Object str) {
        return kit.isBlank(str);
    }
}

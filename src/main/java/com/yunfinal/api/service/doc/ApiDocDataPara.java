package com.yunfinal.api.service.doc;

/**
 * 参数文档对象
 *
 * @author 杜福忠 2019-03-08 18:20:55
 */
public class ApiDocDataPara {
    // 参数名
    public String name;
    // 必选
    public boolean notNull;
    // 类型
    public String type;
    // 解释说明
    public String explain;
    //子参数
    public Object value;

    public ApiDocDataPara(String name, boolean notNull, String type, String explain, Object value) {
        this.name = name;
        this.notNull = notNull;
        this.type = type;
        this.explain = explain;
        this.value = value;
    }

    @Override
    public String toString() {
        return "ApiDocDataPara{" +
                "name='" + name + '\'' +
                ", notNull=" + notNull +
                ", type='" + type + '\'' +
                ", explain='" + explain + '\'' +
                //", value=" + value +
                '}';
    }
}
